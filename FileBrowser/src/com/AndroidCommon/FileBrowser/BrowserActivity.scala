package com.AndroidCommon.FileBrowser

import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConversions._

import _root_.java.io.File
import _root_.java.util.{List, Collections}
import _root_.java.sql.Date
import _root_.java.text.DateFormat

import _root_.android.content.{Context, Intent}
import _root_.android.app.AlertDialog
import _root_.android.app.{Activity, ListActivity}
import _root_.android.view.{View, ViewGroup, LayoutInflater}
import _root_.android.widget._
import _root_.android.graphics.drawable.Drawable
import _root_.android.os.Bundle

import com.AndroidCommon.Misc._

class BrowserActivity extends ListActivity with ActivityUtils{


  /**Clase implicita para poder usar cosas de scala con los files
   *
   * Usando cosas de scala como Option nos permite asegurar que no vamos a 
   * usar por accidente cosas como punteros nulos
   * */
  implicit class MyFile(f: File){

    def listFilesOption():Option[Array[File]] = f.listFiles() match {
      case null => None
      case x    => Some(x)
    }

  }


  implicit val TAG = LogTag("BrowserActivity")


  val RESULT_OK: Int = 1
  val RESULT_BAD: Int = 2


  var dirActual:File = _
  val formato: DateFormat = DateFormat.getDateTimeInstance()
  var adapter: BrowserArrayAdapter = _ 

  override def onCreate(bundle: Bundle) {
    super.onCreate(bundle)

    dirActual = new File("/sdcard/")

    if(dirActual != null) llenar(dirActual)
  }

  /******************
   * Obtiene los elementos de la carpeta
   * @param File El directorio del cual se quiere saber el contenido que tiene
   */
  private def llenar(f: File) = {

    val dirs: Option[Array[File]] = f.listFilesOption()
    var x = 0
    var dir: ArrayBuffer[Item] = ArrayBuffer()
    var arch: ArrayBuffer[Item] = ArrayBuffer()

    val msj:String = getResources() getString(R.string.browser_current_dir)
    

    this.setTitle(msj + " " + f.getName())

    dirs match {
      case Some(a) => llenarConArchivos(a)
      case _       => mostrarError()
    }

    /**Llena con archivos el adapter
     *
     * @param dirs Un array que contiene los archivos del directorio
     * */
    def llenarConArchivos(dirs: Array[File]): Unit = {
      for(x <- 0 to (dirs.length - 1)){
        analizar(dirs(x), dir, arch)
      }

      log("largo dir: " + dir.length)
      dir.sorted
      arch.sorted
      dir.appendAll(arch)

      if( !f.getName().equalsIgnoreCase("sdcard"))
        dir.insert(0, new Item("..", "Parent Directory", "", f.getParent(),"directory_up"));

      adapter = new BrowserArrayAdapter(BrowserActivity.this, R.layout.browseritem, dir.toArray)
      this setListAdapter adapter
    }

    def mostrarError(): Unit = {
      new AlertDialog.Builder(this)
        .setTitle("Error")
        .setMessage("Error opening file or folder")
        .setCancelable(true)
        .show
    }

  }
  /** A un elemento (puede ser directorio o archivo) le crean un objeto Item, 
   * con algunos datos del elementos como la fecha de modificacion entre otros
   * */
  def analizar(f:File, dir: ArrayBuffer[Item], arch: ArrayBuffer[Item]) = {
    val fecha: String = formatearFecha(f)

    if( f.isDirectory()){
      dir += procesarDir(f, fecha)
    }
    else{
      arch += procesarArchivo(f, fecha)
    }
  }

  def formatearFecha(f: File):String = formato format new Date(f.lastModified())
 

  def procesarDir(f:File, fecha:String):Item = {
    val fbuf = f.listFiles()
    var buf = 0

    if(fbuf != null){
      buf = fbuf.length
    } 

    var num_item = String.valueOf(buf)

    if(buf == 0) num_item = num_item + " item"
    else num_item = num_item + " items"

    new Item(f.getName(), num_item, fecha, f.getAbsolutePath(), "directory_icon")
  }

  def procesarArchivo(f: File, fecha: String): Item = {
    new Item(f.getName(), f.length() + " Byte",fecha,f.getAbsolutePath(), "file_icon" )
  }

  override def onListItemClick(l: ListView, v:View, position: Int, id: Long) = {
    super.onListItemClick(l, v, position, id)
    val o: Item = adapter.getItem(position)
    log("click en: " + o.getName())

    if(o.getImage().equalsIgnoreCase("directory_icon") || o.getImage().equalsIgnoreCase("directory_up")){
      dirActual = new File(o.getPath())
      llenar(dirActual)
    }
    else {
      onFileClick(o)
    }

  }
  def onFileClick(o: Item) = {
    //Toast.makeText(this, "Folder Clicked: "+ currentDir, Toast.LENGTH_SHORT).show();
    val intent: Intent = new Intent()
    intent.putExtra("DIR",dirActual.toString())
    intent.putExtra("DIR_NOMBRE",o.getName())
    setResult(RESULT_OK, intent)
    finish()
  }

}


class BrowserArrayAdapter (ctx: Context, id: Int, objeto: Array[Item]) extends ArrayAdapter(ctx, id , objeto) with CustomTypedResource {

  override def getItem(i:Int) = objeto(i)
  
  override def getView(posicion: Int, convertView: View, parent: ViewGroup): View = {

    var v:View = convertView

    if (convertView == null) {
      v = inflar()
    }

    val elem:Item = objeto(posicion)

    if( elem != null)
      rellenar(elem, v)
    v
  }

  /*******
   */

  def inflar():View = {
    ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) match{
      case i: LayoutInflater => i.inflate(id,null)
    }
  }

  /*******
   */
  def rellenar(o: Item, v: View) = {

    if(o != null){
      val t1:TextView = findView(TR.BE_TextView01, v)
      val t2:TextView = findView(TR.BE_TextView02, v)
      val t3:TextView = findView(TR.BE_TextViewDate, v)
      val imagen:ImageView = findView(TR.BE_Icon1, v)

      val uri: String = "drawable/" + o.getImage()
      val imageResource: Int = ctx getResources() getIdentifier(uri, null, ctx.getPackageName)
      val dibujo: Drawable = ctx getResources() getDrawable(imageResource)
      imagen setImageDrawable(dibujo)

      if( t1 != null) t1 setText(o.getName)
      if( t2 != null) t2 setText(o.getDate)
      if( t3 != null) t3 setText(o.getData)
    }
  }

}
/***********
 *
 */
class Item(name: String, data: String, date: String, path: String, imagen: String) extends Comparable[Item]{

  def getName() = name
  def getData() = data
  def getDate() = date
  def getPath() = path
  def getImage() = imagen

  def compareTo(o: Item): Int = {
    if(name != null ){
      return name toLowerCase() compareTo(o getName() toLowerCase())
    }
    else throw new IllegalArgumentException()
  }
}
