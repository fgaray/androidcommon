package com.AndroidCommon.FileBrowser

import _root_.android.view.View


trait CustomTypedResource {

  def findView[T](tr: TypedResource[T], v: View) = v.findViewById(tr.id).asInstanceOf[T]

}
