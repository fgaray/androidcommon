import android.Keys._

import android.Dependencies.{apklib,aar}

android.Plugin.androidBuildAar

name := "file-browser"

platformTarget in Android := "android-15"

scalaVersion := "2.10.3"

resolvers += Resolver.file("Local repo", file(System.getProperty("user.home") + "/.ivy2/local"))(Resolver.ivyStylePatterns)

libraryDependencies += aar("misc-libs" % "misc-libs" % "0.1-SNAPSHOT")
