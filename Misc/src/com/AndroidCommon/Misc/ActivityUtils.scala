package com.AndroidCommon.Misc


import _root_.android.app.Activity
import _root_.android.content.Context
import _root_.android.content.Intent
import _root_.android.util.Log
import _root_.android.content.DialogInterface
import _root_.android.os.Bundle
import _root_.android.app.AlertDialog


import language.implicitConversions


trait ActivityUtils extends Activity{


  /**Cambia el activity desde el actual al especificado en el argumento
   *
   * @param c El activity que queremo ejecutar
   *
   * Ej:
   *
   * changeActivity(classOf[LoadActivity])
   *
   * */
  def changeActivity[T](c: Class[T]): Unit = startActivity(new Intent(this, c))


  /**Cambia el activity desde el actual al especificado en el argumento, 
   * esperando un resultado
   *
   * @param c El activity que queremo ejecutar
   * @param msj mensaje que se envia a la activity
   *
   * Ej:
   *
   * changeActivity(classOf[LoadActivity], RESULT_OK)
   *
   **/
  def changeActivity[T](c: Class[T], msj: Int): Unit= startActivityForResult(new Intent(this, c), msj)




  /**Cambia el activity desde el actual pasando el objeto a la nueva activity. 
   * La unica condicion es que el objeto a pasar debe ser Serializable, pero 
   * podemos utilizar el trair Serializable.
   * Más info: 
   *  > http://www.scala-lang.org/api/current/index.html#scala.Serializable
   *
   * @param c El activity que queremos ejecutar
   *
   * @param tag El tag que le queremos dar al objeto. Luego utilizamos este
   * string para obtener de vuelta el objeto original
   *
   * @param obj Un objeto serializable para ser pasado al otro activity
   *
   * */
  def changeActivity[T](c: Class[T], tag: String, obj: Serializable): Unit = {
    val intent = new Intent(this, c)
    intent.putExtra(tag, obj)
    startActivity(intent)
  }




  /**Cambia el activity desde el actual pasando el objeto a la nueva activity. 
   * La unica condicion es que el objeto a pasar debe ser Serializable, pero 
   * podemos utilizar el trair Serializable.
   * Más info: 
   *  > http://www.scala-lang.org/api/current/index.html#scala.Serializable
   *
   * Esta funcion por defecto asigna una tag al argumento pasado
   *
   * @param c El activity que queremos ejecutar
   *
   * @param obj Un objeto serializable para ser pasado al otro activity
   *
   * */
  def changeActivity[T](c: Class[T], obj: Serializable): Unit = changeActivity(c, "ARGS", obj)

  

  /**Obtiene el argumento pasado al activity utilizando la tag dada. 
   *
   *  @param tag El tag con el cual vamos a obtener el objeto
   *
   *  @return Un objeto que haya sido pasado como argumento
   * */
  def getExtraArgument(tag: String): Option[Object] = {
    val extras = this.getIntent().getExtras()

    if(extras != null) Some(extras.get(tag)) else None
  }


  /**Obtiene el argumento pasado al activity utilizando una tag por defecto
   *
   *  @return Un objeto del tipo T
   *
   * */
  def getExtraArgument[T]: T = getExtraArgument("ARGS") match {
    case None => throw new NullPointerException
    case Some(obj) => obj.asInstanceOf[T]
  }



  /**Escribe en el log de debug la cadena dada utilizando la tag implicita
   *
   * @param l La string a mostrar por pantalla
   *
   * */
  def log(l: String)(implicit tag: LogTag): Unit = tag match {
    case LogTag(s) => Log.d(s, l)
  }



  def showErrorDialog(msg: String, errorString: String, f: () => Unit): Unit = {
    new AlertDialog.Builder(this)
      .setTitle("Error")
      .setMessage(msg)
      .setCancelable(false)
      .setNeutralButton(errorString, (d: DialogInterface, i: Int) => {
        f
      }).show
  }


  /**Obtiene un recurso string a partir del id dado
   *
   * @param id El id del elemento que se quiere obtener el string
   *
   * Ej: 
   *
   * rString(R.string.id_string
   * */
  def rString(id: Int): String = getResources().getString(id);


  /**Nos permite ejecutar la funcion f dada de forma facil en un hilo separado.
   *
   * @param f Una funcion que se va a ejecutar en un nuevo hilo, recibe el hilo
   * que esta ejecutando la funcion
   *
   * */
  def async(f: (Thread) => Unit): Unit = {
    new Thread(new Runnable() {
        def run() = {

          log("Ejecutando la funcion en un nuevo hilo")(LogTag("Hilo"))

          f(Thread.currentThread())
        }
  }).start
  }



  /**Este conversor implicito permite poder ejecutar funciones en el hilo
   * principal de la aplicacion.
   *
   * Ej: Permite hacer:
   *
   *  runOnUiThread{ FUNCIONES }
   *
   * Requiere: import language.implicitConversions
   * */
  implicit def toRunnable[F](f: => F): Runnable = new Runnable(){
    def run() = f
  }



  /**Este conversor implicito nos permite usar una funcion como listener
   * cuando el usuario hace click sobre un boton
   *
   * @param dialog El dialogo que llama a la funcion
   * @param id El botono que esta llamando a la funcion
   *
   *
   *
   * */
  implicit def toOnClickListener[F](f: (DialogInterface, Int) => F): DialogInterface.OnClickListener = new DialogInterface.OnClickListener(){

    override def onClick(dialog: DialogInterface, id: Int) = f(dialog, id)

  }





}


/**Este tipo de dato almacena el tag que vamos a usar para mostrar los 
 * logs en el logcat*/
case class LogTag(t: String)



