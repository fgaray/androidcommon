Librerias varias para trabajar con android creadas por:

- Felipe Garay
- Patricio Jara


# General

Para compilar las librerias se necesita de sbt y un compilador de scala, además
de tener la variable de entorno ANDROID_HOME apuntando a la carpeta donde se
encuentran los binarios de android.

Para crear un archivo apk utilice el comando dentro de la consola de sbt

```
    android:package-debug
```

Para exportar la libreria al repositorio local utilice


```
    publish-local
```

Luego puede incluir las liberias en su proyecto usando:

```
    resolvers += Resolver.file("Local repo", file(System.getProperty("user.home") + "/.ivy2/local"))(Resolver.ivyStylePatterns)

    libraryDependencies += aar("NOMBRE" % "NOMBRE" % "VERSION")
```

Por ejemplo para incluir la liberia misc:


```
    resolvers += Resolver.file("Local repo", file(System.getProperty("user.home") + "/.ivy2/local"))(Resolver.ivyStylePatterns)

    libraryDependencies += aar("misc-libs" % "misc-libs" % "0.1-SNAPSHOT")
```



# File Browser

Esta libreria permite utilizar un navegador de archivos desde una aplicación.


## Dependencias

Requiere de las liberias misc las cuales deben estar disponibles en el
repositorio local.


# Misc

Estas son unas clases en scala que nos ayudan a realizar algunas tareas básicas.
La mayoria son wrappers de clases de java pero con algunas funcionalidades
extras que no vienen por defecto.
